package todo

import (
	"net/http"
	"time"
)

//The object to return
type Todo struct {
	Id   int `json:"id"`
	Name string `json:"name"`
	Due  time.Time `json:"due"`
}

//The responseHandler is implemented in the router gofile and is responsible for writing ResponseBody data to the client
type ResponseHandler func(http.ResponseWriter, *http.Request, ResponseBody);


type Response func(*http.Request) (ResponseBody)

//The ResponseBody must be returned by a receiver function and will be interpreted by the ResponseHandler.
//The ResponseHandler gets all the data it needs for sending a response to the client from this struct
type ResponseBody struct {
	Body           interface{} `json:"body"`
	Meta           MetaBody `json:"meta"`
	HttpStatusCode int `json:"-"`
	Error          interface{} `json:"error"`
}

//region so meta
//Some basic structs to act as meta objects
type MetaBody struct {
	Selection Selection `json:"selection"`
	Database Database `json:"database,omitempty"`
}

type Selection struct {
	Offset int `json:"offset"`
	Limit int `json:"limit"`
	Total int `json:"total"`
}

type Database struct {
	NumQueries int `json:"num-queries,omitempty"`
	QueryLog []string `json:"query-log,omitempty"`
	TotalTime float64 `json:"total-time,omitempty"`
}
//endregion

//A responseWrapper function is basically a function inside a receiver class responsible for processing the request
type ResponseWrapper func(ResponseHandler) http.HandlerFunc


type Todos []Todo

func Create(inner ResponseHandler) http.HandlerFunc {

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		response := ResponseBody{Body:Todo{Name:"CreatedTodo"}, HttpStatusCode:201}
		inner(w, r, response)
	})
}

func FetchAll(inner ResponseHandler) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		todos := Todos{Todo{Name:"Todo1"}, Todo{Name:"Todo2"}, Todo{Name:"Todo3"}, Todo{Name:"Todo4"}, Todo{Name:"Todo5"}}

		metaBody := MetaBody{Selection:Selection{Offset:0,Limit:20,Total:4000}}
		metaBody.Database.NumQueries = 2
		metaBody.Database.TotalTime = 0.002
		metaBody.Database.QueryLog = []string{"SELECT * FROM A","SELECT * FROM B","SELECT * FROM B","SELECT * FROM B","SELECT * FROM B","SELECT * FROM B","SELECT * FROM B","SELECT * FROM B","SELECT * FROM B","SELECT * FROM B","SELECT * FROM B","SELECT * FROM B"}
		response := ResponseBody{Body:todos, HttpStatusCode:200,Meta:metaBody}
		inner(w, r, response)
	})
}

func Fetch(inner ResponseHandler) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		response := ResponseBody{Body:Todo{Name:"Todo1"}, HttpStatusCode:200}
		inner(w, r, response)
	})
}

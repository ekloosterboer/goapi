package main

import "time"

/*
 * Simple api object, unused
 */
type Todo struct {
	Id int `json:"id"`
	Name      string `json:"name"`
	Completed bool `json:"completed"`
	Due       time.Time `json:"due"`
}

type Todos []Todo

package main

import (
	"net/http"
	"log"
)

func main() {
	//Create our own version of the router
	router := NewRouter()
	log.Fatal(http.ListenAndServe("localhost:3001", router))
}


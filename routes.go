package main

import (
	"bitbucket.org/ekloosterboer/goapi/rest/receiver/todo"
)

type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc todo.ResponseWrapper
}

type Routes []Route

//All defined routes in the api
var routes = Routes{
	Route{
		"Todo",
		"POST",
		"/todo",
		todo.Create,
	},
	Route{
		"Todo",
		"GET",
		"/todo/{id}",
		todo.Fetch,
	},
	Route{
		"Todo",
		"GET",
		"/todo",
		todo.FetchAll,
	},
}

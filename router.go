package main

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/ekloosterboer/goapi/rest/receiver/todo"
	"github.com/gorilla/mux"
)

func NewRouter() *mux.Router {
	router := mux.NewRouter().StrictSlash(true)

	/*
		Loop over all the different routes defined in the routes file and register them on the mux router.
	*/
	for _, route := range routes {
		//add this handler to the Handler to log the requests to the console
		handler := Logger(route.HandlerFunc(HandleResponse), route.Name)

		router.Methods(route.Method).Path(route.Pattern).Name(route.Name).Handler(handler)

	}
	return router
}

/*
 * This is the final responsehandler of the api. this is the only function responsible for writing to the responseWriter (client output).
 */
func HandleResponse(w http.ResponseWriter, r *http.Request, responseBody todo.ResponseBody) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(responseBody.HttpStatusCode)
	json.NewEncoder(w).Encode(responseBody)
}
